import pandas as pd
import numpy as np
import time
import re

INPUT_DATA = 'data/input_data.csv'

def get_first_rows(amount = 10, filename = INPUT_DATA):
    df = pd.read_csv(filename, sep = ';')
    return df.iloc[:amount] # df.head()

def list_column_names(filename = INPUT_DATA):
    df = pd.read_csv(filename, sep = ';')
    return list(df.columns.values)



ADDRESSES = "data/addresses.csv"
TERYT = "data/teryt.csv"
FOUND_RESULTS = "data/znalezione_rekordy.csv"
UNFOUND_RESULTS = "data/nieznalezione_rekordy.csv"
ILLEGAL_COMPONENTS = ["al.", "al", "aleja", "ul.", "ul", "ulica"]
addresses_csv = pd.read_csv(ADDRESSES, ";")



def get_street_and_number_for_address(address):
    array = address.split()
    number = array.pop()
    street = " ".join(array)

    return (street, number)


def get_id_for_street(street):
    teryt = pd.read_csv(TERYT, ";")

    search_text = ""
    matrix = False
    for component in reversed(street.split()):
        if component in ILLEGAL_COMPONENTS:
            break
        if (search_text == ""):
            search_text = component
        else:
            search_text = component + " " +  search_text


        # print("search_text " + search_text)
        # Series.str.contains(pat, case=True, flags=0, na=nan, regex=True)[source]
        regex = re.compile(search_text + "$", re.IGNORECASE)
        test = teryt[teryt["NAZWA_1"].str.contains(regex)==True]
        # print(test)
        matrix = test.as_matrix(["SYM_UL"])
        size = matrix.size
        # print("Found " + str(size) + " streets")
        if (size == 1):
            return matrix[0][0]

    if (matrix.size > 0):
        temp = False
        for item in matrix:
            if (temp == False):
                temp = item[0]
            elif temp != item[0]:
                return False
        return temp

    return False

def get_coordinates_for_street_and_number(street, number):
    id = str(get_id_for_street(street))
    a = addresses_csv[addresses_csv["TERYT_KOD"].str.contains(id)==True]
    regex = re.compile("^" + str(number) + "$", re.IGNORECASE)
    a = a[a["ADR_NR"].str.contains(regex)==True].as_matrix(["GEOM_X", "GEOM_Y"])
    if (a.size > 0):
        coord = a[0]
        # print(coord[0], coord[1])
        return (coord[0], coord[1])

    return (0, 0)


def normalize_street(street):
    wo_spaces = re.sub(r'\s+', ' ', street.strip())
    return re.sub(r'(os.|al.|ul.|pl.)', '', wo_spaces).strip()

def find_coordinates(columns, single = False):
    df = pd.read_csv(INPUT_DATA, sep = ';')
    unfound_df = df
    found_dict = []
    unfound_dict = []
    our_cols = df[columns]
    print(len(our_cols.columns))
    a = 0
    i = 0
    columns = []
    for row in df:
        columns.append(row)

    for row in df.itertuples(index = False):
        street = ""
        number = ""
        temp_found_dict = {}
        i = 0
        
        for column in columns:
            
            if (column == "UL"):
                street = row[i]
            elif (column == "NR_BUD"):
                number = row[i]
            temp_found_dict[column] = row[i]
            i += 1
        coords = get_coordinates_for_street_and_number(normalize_street(street), number)
        if (coords == (0, 0)):
            unfound_dict.append(temp_found_dict)
        else:
            temp_found_dict["GEOM_X"] = coords[0]
            temp_found_dict["GEOM_Y"] = coords[1]
            found_dict.append(temp_found_dict)
        
    found_df = pd.DataFrame(found_dict)
    found_df.to_csv(FOUND_RESULTS, index = False)

    unfound_df = pd.DataFrame(unfound_dict)
    unfound_df.to_csv(UNFOUND_RESULTS, index = False)

    return (len(found_dict), len(unfound_dict))

def save_txt_file(start_date, end_date, duration, results_count):
    f = open('data/wyniki.txt','w')
    f.write('Data rozpoczecia procesu geokodowania: '+str(start_date))
    f.write('\n')
    f.write('Data zakonczenia procesu geokodowania: '+str(end_date))
    f.write('\n')
    f.write('Czas geokodowania: ' + str(duration))
    f.write('\n')
    f.write('Liczba znalezionych rekordów: ' + str(results_count[0]))
    f.write('\n')
    f.write('Liczba nieznalezionych rekordów: ' + str(results_count[1]))
    f.close()

#     data i czas rozpoczęcia procesu geokodowania ( z dokładnością do 1 sekundy)
# data i czas zakończenia procesu geokodowania ( z dokładnością do 1 sekundy)
# czas geokodowania w  sekundach
# liczbę dopasowanych rekordów
# liczbę niedopasowanych rekordów

# def main():
    # print(normalize_street('JERZEGO FEDKOWICZA 8I<'))

if __name__ == '__main__':
    start_time = time.time()
    start_date = time.strftime("%Y-%m-%d %H:%M:%S")
    results_count = find_coordinates(['UL', 'NR_BUD'])
    end_date = time.strftime("%Y-%m-%d %H:%M:%S")
    end_time = time.time()

    duration = end_time-start_time
    save_txt_file(start_date, end_date, duration, results_count)
    print('Time of execution: {:.3} seconds'.format(time.time() - start_time))
