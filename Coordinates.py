import pandas as pd
import numpy as np
import re

ADDRESSES = "data/addresses.csv"
TERYT = "data/teryt.csv"
ILLEGAL_COMPONENTS = ["al.", "al", "aleja", "ul.", "ul", "ulica"]
addresses_csv = pd.read_csv(ADDRESSES, ";")

def get_id_for_street(street):
    teryt = pd.read_csv(TERYT, ";")
    teryt["x"] = pd.Series("test123")
    teryt.to_csv("data/test.csv")
    search_text = ""
    matrix = False
    for component in reversed(street.split()):
        if component in ILLEGAL_COMPONENTS:
            break
        if (search_text == ""):
            search_text = component 
        else:
            search_text = component + " " +  search_text
                
        
        print("search_text " + search_text)
        regex = re.compile(search_text + "$", re.IGNORECASE)
        test = teryt[teryt["NAZWA_1"].str.contains(regex)==True]

        matrix = test.as_matrix(["SYM_UL"])
        size = matrix.size
        print("Found " + str(size) + " streets")
        if (size == 1):
            return matrix[0][0]

    if (matrix.size > 0):
        temp = False
        for item in matrix:
            if (temp == False):
                temp = item[0]
            elif temp != item[0]:
                return False
        return temp

    return False

def get_coordinates_for_street_and_number(street, number):
    id = str(get_id_for_street(street))
    a = addresses_csv[addresses_csv["TERYT_KOD"].str.contains(id)==True]
    regex = re.compile("^" + str(number) + "$", re.IGNORECASE)
    a = a[a["ADR_NR"].str.contains(regex)==True].as_matrix(["GEOM_X", "GEOM_Y"])
    if (a.size > 0):
        coord = a[0]
        return (coord[0], coord[1])
    
    return (0, 0)

def get_street_and_number_for_address(address):
    array = address.split()
    number = array.pop()
    street = " ".join(array)

    return (street, number)

if __name__ == '__main__':

    test_address = ["lea 20", "aleja jana pawła II avs"]
    for address in test_address:
        street, number = get_street_and_number_for_address(address)
        x, y = get_coordinates_for_street_and_number(street, number)
        print("x " + str(x) + " y " + str(y))
        
            
